
#!/usr/bin/python

#les imports
import sys
import time
import paho.mqtt.publish as publish
import serial


MQTT_SERVER = "ADDRESS"
MQTT_PATH = "TOPIC"
raspberry = "RASPBERRY_ID"
sensor = "SENSOR_ID"
pin = PIN
interval = INTERVAL

type1 = "DATA_TYPE_1"
type2 = "DATA_TYPE_2"
type3 = "DATA_TYPE_3"

if __name__ == '__main__':
    ser = serial.Serial('/dev/ttyACM0', 9600, timeout=1)
    ser.flush()

while True:
    valeur1 = 0
    valeur2 = 0
    valeur3 = 0
    if ser.in_waiting > 0:
        line = ser.readline().decode('utf-8').rstrip()
        valeur1=line[10:3]; 
        valeur2=line[26:3];
        valeur3=line[37:1];


    string = '{"raspberry" : %s , "sensor" : %s , "data" : [ { "%s" : %s }, { "%s" : %s }, { "%s" : %s } ] }'%(raspberry ,sensor, type1, valeur1, type2, valeur2, type3, valeur3)

    publish.single(MQTT_PATH, string, hostname=MQTT_SERVER)

    time.sleep(interval)
