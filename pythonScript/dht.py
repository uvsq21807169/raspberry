import sys
import time

import Adafruit_DHT
import paho.mqtt.publish as publish


MQTT_SERVER = "ADDRESS"
MQTT_PATH = "TOPIC"
raspberry = "RASPBERRY_ID"
sensor = "SENSOR_ID"
pin = PIN
interval = INTERVAL

type1 = "DATA_TYPE_1"
type2 = "DATA_TYPE_2"

while True:
    valeur2, valeur1 = Adafruit_DHT.read_retry(11, pin)        

    string = '{"raspberry" : %s , "sensor" : %s , "data" : [ { "%s" : %s }, { "%s" : %s } ] }' %(raspberry ,sensor, type1, valeur1, type2, valeur2)

    if(valeur1 and valeur2):
          publish.single(MQTT_PATH, string, hostname=MQTT_SERVER)
          
    time.sleep(interval)
