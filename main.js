const electron = require("electron");
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const  ipc = electron.ipcMain;

const http = require('http');
const fs  = require('fs');
const NodeSSH = require('node-ssh')
const mqtt = require('mqtt');
const csv = require('fast-csv');
const async = require('async');


const serverIpAddress = 'localhost'
const serverPort = 3000

const mqtt_topic = "RB";
const mqtt_port = 1883;

var local_address;
var mainWindow;
var sensorWindow;

const httpOptions = {
  hostname: serverIpAddress,
  port: serverPort,
  path: '/upload',
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  }
};


function createWindow () {


  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true
    }
  })

  // and load the index.html of the app.
  mainWindow.loadFile('./views/index.html')
  // Open the DevTools.
  //win.webContents.openDevTools()

  activateRules();

}



// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})


var os = require('os');
var ifaces = os.networkInterfaces();
Object.keys(ifaces).forEach(function (ifname) {
  ifaces[ifname].forEach(function (iface) {
    if ('IPv4' !== iface.family || iface.internal !== false) {
      // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
      return;
    }
    local_address =  iface.address

  });
});


function sendRequest(fun, name, callback, fields){

  fields = fields || {};

  const req = http.request(httpOptions, (res) => {

    //console.log(`STATUS: ${res.statusCode}`);
    //console.log(`HEADERS: ${JSON.stringify(res.headers)}`);

    res.setEncoding('utf8');
    let data = [];
    res.on('data', (chunk) => {
      data.push(Buffer.from(chunk, 'utf8'));
    });

    res.on('end', () => {
       data = Buffer.concat(data);
       data = JSON.parse(data);
       callback(data);
    });

  });

  req.on('error', (e) => {
    console.error(`problem with request: ${e.message}`);
  });

  let request = JSON.stringify({"function" : fun , "name" : name, "fields" : fields});
  req.write(request);
  req.end();

}


var current_raspberry;
ipc.on('open-sensors-window', (event, data) => {

  current_raspberry = data;

  sendRequest('get' , 'getRaspberries', (fields) => {
   sensorWindow = new BrowserWindow({
     height: 300,
     width: 500,
     parent : mainWindow,
     //modal : true,
     webPreferences: {
       nodeIntegration: true
     },
     title : fields[0].designation
   });

   sensorWindow.loadFile('./views/sensors.html');

   sensorWindow.on('closed', () => {
      sendRaspList(event);
      sensorWindow = null
    })

 }, {'id' : data});

});



ipc.on('load-raspberries', (event) => {
  sendRaspList(event);
})

ipc.on('add-raspberry', (event, data) => {

  sendRequest('set' , 'addRaspberry', (data) => {
                  sendRaspList(event);
              }, data );

})

ipc.on('remove-raspberry', (event, id) => {
  sendRequest('delete' , 'deleteRaspberry', (data) => {
                  sendRaspList(event);
              }, { 'id' : id } );
})

ipc.on('load-sensors', (event) => {

  sendSensorsList(event);

})

ipc.on('remove-sensor', (event, id) => {

  sendRequest('delete' , 'deleteSensor', (data) => {
                  sendSensorsList(event);
              }, { 'id' : id } );
})

ipc.on('add-sensor', (event, fields) => {

    fields['id_raspberry'] = current_raspberry;

    sendRequest('set' , 'addSensor', (sensor_id) => {

                        sendSensorsList(event);

                        sendRequest('get' , 'getRaspberries', (data) => {

                           data = data[0];

                           ssh = new NodeSSH()

                           ssh.connect({
                             host: data.IpAddress,
                             username: data.hostName,
                             password: data.password
                           })
                           .then(function() {

                            sendPythonScripts(ssh, sensor_id);

                           })

                         },{'id' : current_raspberry });

                     }, fields );

})

//this collection contains raspberry id and the object of mqqt client
//in order to stop the sender later on
var mqtt_clients = new Map()
var indexEvent;

ipc.on('activate-raspberry', (event, id) => {

  indexEvent = event;

  sendRequest('update' , 'updateRaspberry', (data) => {

              }, {'id' : id, 'activated' : 1} );

  sendRequest('get' , 'getRaspberries', (data) => {

     data = data[0];

     ssh = new NodeSSH()

     ssh.connect({
       host: data.IpAddress,
       username: data.hostName,
       password: data.password
     })
     .then(function() {

       executePythonScript(ssh, id);

       mqttSubscribe(id , (client) => {

         mqtt_clients.set(id, client);

         console.log("raspberry activated");

         saveData(client);

       });

     })

   },{'id' : id });

});

ipc.on('disactivate-raspberry', (event, id) => {

  sendRequest('update' , 'updateRaspberry', (data) => {

  }, {'id' : id, 'activated' : 0} );

    //i have to kill process in background in raspberry ... python scripts
    mqtt_clients.get(id).end();

});

ipc.on('activate-sensor', (event, id) => {

});

ipc.on('disactivate-sensor', (event, id) => {

});


ipc.on('rule-get-raspberry-list', (event) => {
  sendRequest('get' , 'getRaspberries', (data) => {
     event.sender.send('rule-load-raspberry-list', data);
   });
});
ipc.on('rule-get-sensor-list', (event, fields) => {
  sendRequest('get' , 'getSensors', (data) => {
     let output = [fields[0], data]
     event.sender.send('rule-load-sensor-list', output);
   },{'raspberry' : fields[1] , 'actuator' : fields[0]});
});
ipc.on('rule-get-data-type-list', (event, sensor) => {
  sendRequest('get' , 'getDataTypes', (data) => {
     event.sender.send('rule-load-data-type-list', data);
   },{'sensor' : sensor});
});

ipc.on('load-rules-list', (event) => {

  sendRulesList(event);

});

ipc.on('add-rule', (event, fields) => {

  sendRequest('set' , 'addRule', (data) => {
                  sendRulesList(event);
              }, fields );


});
let activated_rules = [];
ipc.on('activate-rule', (event, id) => {

  sendRequest('update' , 'updateRule', (data) => {
                activated_rules.push(data);
              }, {'id' : id, 'activated' : 1} );
});

ipc.on('disactivate-rule', (event, id) => {
  sendRequest('update' , 'updateRule', (data) => {
                for(i in activated_rules){
                  if (activated_rules[i].id === data.id) {
                    activated_rules.splice(i, 1);
                    break;
                  }
                }
             }, {'id' : id, 'activated' : 0} );
});

function activateRules(){

  sendRequest('get' , 'getRules', (data) => {

    activated_rules = data;

 }, {'activated' : 1});

}

ipc.on('get-data', (event, id) => {

  sendRequest('get' , 'getData', (data) => {
     event.sender.send('show-data', data);
   });
});


let sendPythonScripts = function(ssh, sensor_id){

  sendRequest('get' , 'getSensors', (data) => {

    data.forEach(async function(item) {

      let input = fs.readFileSync('./pythonScript/'+item.type+'.py')+"";

      console.log(item);

      //modification du fichier ...
      //ADDRESS, TOPIC, PIN, INTERVAL, TYPE, DATA_TYPE_1, DATA_TYPE_2
      //TODO : get server ip address
      input = input.replace("ADDRESS", local_address);
      input = input.replace("TOPIC", mqtt_topic+"/"+item.id_raspberry);
      input = input.replace("PIN", item.pin_number);
      input = input.replace("INTERVAL", item.intervalle);
      input = input.replace("RASPBERRY_ID", item.id_raspberry);
      input = input.replace("SENSOR_ID", item.id);



      await new Promise((resolve, reject) => {

        sendRequest('get' , 'getDataTypes', (data_type) => {

          let i = 1;
          data_type.forEach((dt) => {
            input = input.replace("DATA_TYPE_"+i, dt.name);
            i++;
          });

          console.log('---- python script ----');
          console.log(input);
          console.log('-----------------------');

          resolve();
        },{'sensor' : item.id});

      });


      let imports = [];
      var reg = /import (\S+)/g;
      var result;
      while((result = reg.exec(input)) !== null) {
          if (result[1] != 'sys' && result[1] != 'time') {
            imports.push(result[1]);
            console.log(imports);
          }
      }

      console.log(imports);

      //Instaling modules
      /*
      let cmd = 'sudo -S ;'+
                'apt-get install python3-pip ; '+
                'sudo python3 -m pip install --upgrade pip setuptools wheel ; ';
      */
/*
      let cmd = 'python3 -m pip install --upgrade pip setuptools wheel ; ';

      ssh.execCommand(cmd , { stdin: ssh.connection.config.password } ).then( function(result) {

            console.log('STDOUT: ' + result.stdout)
            console.log('STDERR: ' + result.stderr)


            for (var i = 0; i < imports.length; i++) {

                let command = 'pip install '+ imports[i];
                console.log(command);

                ssh.execCommand(command).then( function(result) {
                  console.log('STDOUT: ' + result.stdout)
                  console.log('STDERR: ' + result.stderr)

                })
            }

      })
*/

      let remote_file_name = item.type+'.py';

      ssh.execCommand('mkdir script ; cd script ; cat > '+remote_file_name, { stdin: input } ).then( function(result) {
        console.log('STDOUT: ' + result.stdout)
        console.log('STDERR: ' + result.stderr)

        console.log('----> remote Files : ');
        ssh.execCommand('ls').then( function(result) {
          console.log('STDOUT: ' + result.stdout)
          console.log('STDERR: ' + result.stderr)
        })

      });



    });

  }, {'id' : sensor_id} );

}

function executePythonScript(ssh, sensor_id){
  sendRequest('get' , 'getSensors', (data) => {

    data.forEach( function(item) {

      let remote_file_name = item.type+'.py';

      console.log('python3 '+remote_file_name);

      ssh.execCommand('cd script; python3 '+remote_file_name).then( function(result) {
        console.log('STDOUT: ' + result.stdout)
        console.log('STDERR: ' + result.stderr)
      })

    })

  }, {'raspberry' : sensor_id} )

}




function mqttSubscribe(raspberry_id, callback){

  var client  = mqtt.connect('mqtt://'+serverIpAddress, {port: mqtt_port});
  client.on('connect', function () {
    client.subscribe(mqtt_topic+"/"+raspberry_id)
    callback(client);
  });

}

async function writeToCsv(data){

  let content = new Object();

    fs.createReadStream('live-data.csv')
      .pipe(csv.parse({ headers: true }))
      .on('error', error => console.error(error))
      .on('data', function(row){

        content = row;
      })
      .on('end', function(){

        for ( i in data) {
          content[data[i].key] = data[i].value;
        }

        let ws = fs.createWriteStream('live-data.csv');

        const csvStream = csv.format({ headers: true });

        csvStream.pipe(ws).on('end', process.exit);
        csvStream.write(content);
        csvStream.end();

      });
}

function saveData(client){

  console.log('listening **********');

  //mosquitto_pub -t 'RB' -m 'a;temp:99;humid:23.2'
  client.on('message', function (topic, message) {

    message = message.toString();

    message = JSON.parse(message);

    console.log('message : ');
    console.log(message);

    console.log('activated rules : ');
    console.log(activated_rules);


    //message exemple
    //{raspberry : raspberryID , sensor : sensorID , data : [ { type1 : value1 }, { type2 : vlaue2 } ] }
    //mosquitto_pub -t 'RB/6' -m '{"raspberry" : 6 , "sensor" : 12 , "data" : [ { "temp" : 23 }, { "humid" : 88 } ] }'

    let raspberry = message.raspberry;
    let sensor = message.sensor;

    let data = [];

    for(var m in message.data){
        var val = message.data[m];

        var type = val;
        for(var j in val){
            type = j;
        }
        var value = val[type];

        let key = raspberry+'_'+sensor+'_'+type;
        data.push( {'key' : key, 'value' : value, 'type' : type} );

        verifyRule(raspberry, sensor, type, value);

    }

    writeToCsv(data);




  });

}

function verifyRule(raspberry, sensor, data_type, value){


    for(i in activated_rules){
      let r =  activated_rules[i];

      //if this is the data we are locking for ...
      if (r.raspberry == raspberry && r.sensor == sensor && r.data_type == data_type) {

        switch (r.condi) {
          case '>':
            if (value > r.value) {
              sendReaction(r);
            }
            break;
          case '<':
            if (value < r.value) {
              sendReaction(r);
            }
            break;
          case '>=':
            if (value >= r.value) {
              sendReaction(r);
            }
            break;
          case '<=':
            if (value <= r.value) {
              sendReaction(r);
            }
            break;
          case '=':
            if (value == r.value) {
              sendReaction(r);
            }
            break;
          case '!=':
            if (value != r.value) {
              sendReaction(r);
            }
            break;
          default:
            console.log(' Other condition ! ');

        }

      }
    }
}

function sendReaction(rule){
  console.log('**********************************************************');
  console.log('Action sent ...')
  console.log('**********************************************************');


  let message = 'La règle '+rule.designation+' a été exécuté !!! '
  indexEvent.sender.send('show-pop-up', message);


  sendRequest('get' , 'getRaspberries', (data) => {

     data = data[0];

     ssh = new NodeSSH()

     ssh.connect({
       host: data.IpAddress,
       username: data.hostName,
       password: data.password
     })
     .then(function() {

       sendPythonScripts(ssh, id);

       let remote_file_name = 'script-'+rule.actuatorType+'.py';

       /*
       ssh.execCommand('python '+remote_file_name).then( function(result) {
         console.log('STDOUT: ' + result.stdout)
         console.log('STDERR: ' + result.stderr)
       })
       */

     })

   },{'id' : rule.actuator_rasp });

}

function sendRaspList(event){

   sendRequest('get' , 'getRaspberries', (data) => {

     event.sender.send('show-raspberries-list', data);

  })
}


function sendSensorsList(event){

  sendRequest('get' , 'getSensors', (data) => {

    event.sender.send('show-sensors-list', data);

  }, {'raspberry' : current_raspberry} );

}


function sendRulesList(event){

    sendRequest('get' , 'getRules', (data) => {

       event.sender.send('show-rules-list', data);

   });

}


















;
