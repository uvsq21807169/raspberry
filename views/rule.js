
const electron = require('electron');
const ipc = electron.ipcRenderer;

ipc.send('load-rules-list');

ipc.send('rule-get-raspberry-list');


ipc.on('show-rules-list', (event, data) => {
  var content = '<tr>'+
                  '<th>Nom</th>'+
                  '<th>Etat</th>'+
                  '<th>Règle</th>'+
                '</tr>';


  data.forEach(async function(item) {

    let temp = item.action+' le '+item.actuator+' de '+item.rasp_actuator+' Si la '+item.data_type+' de '+
                 item.sens+' du '+item.rasp +' est '+ item.condi+' à '+ item.value;

    let button_content = item.activated ? 'Désactiver' : 'Activer'

    content += '<tr>'+
                  '<td>'+item.name+'</td>'+
                  '<td><button id="btn-active-rule-'+item.rule_id+'" onclick="rule.activateRule('+item.rule_id+')">'+button_content+'</button></td>'+
                  '<td>'+temp+'</td>'+
                '</tr>';

  });

  document.getElementById('customers').innerHTML = content;
})


function activateRule(id){

  if ( document.getElementById('btn-active-rule-'+id).innerHTML == 'Activer'){
    ipc.send('activate-rule', id);
    console.log("rule activated");
    document.getElementById('btn-active-rule-'+id).innerHTML = 'Désactiver'
  }
  else {

    ipc.send('disactivate-rule', id);
    console.log("rule disactivated");
    document.getElementById('btn-active-rule-'+id).innerHTML = 'Activer'

  }
}

let btn_add_rule = document.getElementById('btn-add-rule');

btn_add_rule.addEventListener("click", () => {
  let fields = {
        designation : rule_name.value,
        raspberry : raspberry_list.value,
        sensor : sensor_list.value,
        data_type : data_type_list.value,
        condition : condition_list.value,
        value : condition_value.value,
        actuator : actionneur_list.value,
        action : action_list.value
  }
  console.log(fields);
  ipc.send('add-rule', fields);

});


let rule_name = document.getElementById('rule-name');
let raspberry_list = document.getElementById('raspberry-list');
let sensor_list = document.getElementById('sensor-list');
let data_type_list = document.getElementById('data-type-list');
let condition_list = document.getElementById('condition-list');
let actionneur_rasp_list = document.getElementById('actionneur-rasp-list');
let actionneur_list = document.getElementById('actionneur-list');
let action_list = document.getElementById('action-list');
let condition_value = document.getElementById('condition-value');



let same_rasp_checked = document.getElementById('same-rasp');
let not_same_rasp_checked = document.getElementById('not-same-rasp');
let actionneur_rasp_list_container = document.getElementById('actionneur-rasp-list-container');
let actionneur_list_container = document.getElementById('actionneur-list-container');
let actionneur_container = document.getElementById('actionneur-container');
let action_list_container = document.getElementById('action-list-container');
let condition_value_container = document.getElementById('condition-value-container');




let content_raspberries_list;


raspberry_list.addEventListener("input", () => {
  if (raspberry_list.value == -1) return;
  let data = [0, raspberry_list.value ]
  ipc.send('rule-get-sensor-list', data);
});

sensor_list.addEventListener("input", () => {
  if (sensor_list.value == -1) return;
  ipc.send('rule-get-data-type-list', sensor_list.value);
});

data_type_list.addEventListener("input", () => {
  if (data_type_list.value == -1) return;
  document.getElementById('condition-list-container').style.display = 'block';
});

condition_list.addEventListener("input", () => {
  if (condition_list.value == -1) return;

  condition_value_container.style.display = 'block';


  let data = [1, raspberry_list.value ]
  ipc.send('rule-get-sensor-list', data);
  actionneur_rasp_list.innerHTML = content_raspberries_list;
});
/*
same_rasp_checked.addEventListener("input", () => {

  let data = [1, raspberry_list.value ]
  ipc.send('rule-get-sensor-list', data);

  actionneur_list_container.style.display = 'block';
  actionneur_rasp_list_container.style.display = 'none';

});

not_same_rasp_checked.addEventListener("input", () => {

  actionneur_list_container.style.display = 'none';
  actionneur_rasp_list_container.style.display = 'block';


});
*/
actionneur_rasp_list.addEventListener("input", () => {
  if (actionneur_rasp_list.value == -1) return;
  let data = [1, actionneur_rasp_list.value ]
  ipc.send('rule-get-sensor-list', data);
});

actionneur_list.addEventListener("input", () => {
  if (actionneur_list.value == -1) return;
  action_list_container.style.display = 'block';
});






ipc.on('rule-load-raspberry-list', (event, data) => {
  console.log(data);
  let content = '<option value="-1">--</option>';
  data.forEach((item) => {
    content += '<option value="'+item.id+'">'+item.designation+'</option>';
  });
  content_raspberries_list = content;
  raspberry_list.innerHTML = content;
})

ipc.on('rule-load-sensor-list', (event, data) => {
  let content = '<option value="-1">--</option>';
  data[1].forEach((item) => {
    content += '<option value="'+item.id+'">'+item.designation+'</option>';
  });
  if (data[0] == 0) {
    sensor_list.innerHTML = content;
    document.getElementById('sensor-list-container').style.display = 'block';
  }
  else {
    actionneur_list.innerHTML = content;
    document.getElementById('actionneur-list-container').style.display = 'block';
  }

})

ipc.on('rule-load-data-type-list', (event, data) => {
  let content = '<option value="-1">--</option>';
  data.forEach((item) => {
    content += '<option value="'+item.name+'">'+item.name+'</option>';
  });
  data_type_list.innerHTML = content;
  document.getElementById('data-type-list-container').style.display = 'block';
})


module.exports.activateRule = activateRule;
