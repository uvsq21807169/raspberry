
const electron = require('electron');
const ipc = electron.ipcRenderer;



ipc.send('load-raspberries');



let openSensorWindow = function (raspberry){

  ipc.send('open-sensors-window', raspberry);

}

ipc.on('show-raspberries-list', (event, data) => {

  var content = '<tr>'+
                  '<th>Désignation</th>'+
                  '<th>Localisation</th>'+
                  '<th>Nombre de capteurs</th>'+
                '</tr>';

  data.forEach((item) => {

     content += '<tr id="'+item.id+'">'+
                   '<td>'+item.designation+'</td>'+
                   '<td>'+item.location+'</td>'+
                   '<td>'+item.nbr_sensor+'</td>'+
                 '</tr>';

  });
  document.getElementById('customers').innerHTML = content;

})

ipc.on('show-pop-up', (event, message) => {

  alert(message)

})
let activateRaspberry = function(id){
  console.log(id);
  ipc.send('activate-raspberry', id);
  /*
  if ( document.getElementById('btn-active-rasp-'+id).innerHTML == 'Activer'){
    ipc.send('activate-raspberry', id);
    console.log("raspberry activated");
    document.getElementById('btn-active-rasp-'+id).innerHTML = 'Désactiver'
  }
  else {

    ipc.send('disactivate-raspberry', id);
    console.log("raspberry disactivated");
    document.getElementById('btn-active-rasp-'+id).innerHTML = 'Activer'

  }
  */
}

let removeRaspberry = function(id){

  ipc.send('remove-raspberry', id);

  console.log("raspberry deleted");
}


let posts = document.getElementById('customers');
posts.addEventListener('click',(e) => {
// anything you need here, for example:
   e.target.parentNode.classList.add("actif");
    e.preventDefault();
});

document.getElementById('activate').addEventListener('click',(e) => {
    activateRaspberry(document.getElementsByClassName("actif")[0].id);
});

document.getElementById('supp').addEventListener('click',(e) => {
    removeRaspberry(document.getElementsByClassName("actif")[0].id);
});

document.getElementById('voir').addEventListener('click',(e) => {
    window.location.href = "sensors.html";
});

document.getElementById('btn-add-raspberry').addEventListener("click", () => {

  let designation = document.getElementById('designation').value;
  let hostName = document.getElementById('hostName').value;
  let IpAddress = document.getElementById('address-ip').value;
  let password = document.getElementById('input-password').value;
  let location = document.getElementById('location').value;

  let fields = { 'designation' : designation,
                 'hostName' : hostName,
                 'IpAddress' : IpAddress,
                 'password' : password ,
                 'location' : location
               };

  ipc.send('add-raspberry', fields);

});


module.exports.openSensorWindow = openSensorWindow;
module.exports.activateRaspberry = activateRaspberry;
module.exports.removeRaspberry = removeRaspberry;
















;
