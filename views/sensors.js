const electron = require('electron');
const ipc = electron.ipcRenderer;


ipc.send('load-sensors');




let is_actuator = 0;

let actionneur_checked = document.getElementById('actionneur-checked');
let not_actionneur_checked = document.getElementById('not-actionneur-checked');

ipc.on('show-sensors-list', (event, data) => {

    var content="<tr>\n" +
        "                          <th>Rôle</th>\n"+
        "                          <th>Nom</th>\n" +
        "                          <th>Type</th>\n" +
        "                          <th>Numéro Pin</th>\n" +
        "                          <th>Intervale d'envoi</th>\n" +
        "                          <th>Etat</th>\n" +
        "                          <th>Supprimer</th>\n" +
        "                      </tr>";

    data.forEach((item) => {
        content += "<tr>";
        if(item.actuator==0) content+=  "<td> Capteur </td>";
        else content+=  "<td> Actionneur </td>";
            content+= "<td>"+item.designation+"</td>"+
            "<td>"+item.type+"</td>"+
            "<td>"+item.pin_number+"</td>"+
            "<td>"+item.intervalle+" s</td>"+
            "<td>";
            if(item.activated==1) content+= "<label class=\"switch\">\n" +
                "  <input type=\"checkbox\" checked>\n" +
                "  <span class=\"slider round\"></span>\n" +
                "</label>";
            else content+= "<label class=\"switch\">\n" +
                "  <input type=\"checkbox\" onclick='sensor.activateSensor("+item.id+")'>\n" +
                "  <span class=\"slider round\"></span>\n" +
                "</label>\"</td>\"";


            content+= "<td>"+
            "<img src=\"https://img.icons8.com/flat_round/64/000000/delete-sign.png\" style=\"width:16px; height:16px;\" onclick='sensor.removeSensor("+item.id+")' >"+
            "</td>"+
            "</tr>";

    });
  document.getElementById('customers').innerHTML = content;
})


document.getElementById('btn-add-sensor').addEventListener("click", () => {

  const fs = require('fs');

  let designation = document.getElementById('designation').value;
  let type = document.getElementById('type').value;
  let pin = document.getElementById('Numéro-pin').value;
  let interval = document.getElementById('interval').value;


  fs.exists('./pythonScript/'+type+'.py', (result) =>{

     if (result || uploadFile(type)) {

       let nbr_data_type = document.getElementById("nbr-data-type");
       let data_type = [];
       for (var i = 1; i <= nbr_data_type.value; i++) {
         data_type.push(document.getElementById("data-type-"+i).value)
       }

       let fields = { 'data_type' : data_type,
                      'designation' : designation,
                      'type' : type,
                      'pin_number' : pin,
                      'intervalle' : interval,
                      'actuator' : is_actuator
                    };
        console.log(fields);
       ipc.send('add-sensor', fields);
       document.getElementById('file-container').style.display = "none";

     }
     else {
       document.getElementById('file-container').style.display = "block";
     }
   })

})

let removeSensor = function(id){

  ipc.send('remove-sensor', id);

  console.log("sensor deleted");
}

function uploadFile(type){

  var file = document.getElementById("script-input").files[0];

  if (file) {
      var reader = new FileReader();
      reader.readAsText(file, "UTF-8");
      reader.onload = function (evt) {
          const fs = require('fs')

          let data = evt.target.result

          // Write data in 'Output.txt' .
          fs.writeFile('./pythonScript/'+type+'.py', data, (err) => {

              if (err) return false;
          })
      }
      reader.onerror = function (evt) {
          document.getElementById("error-msg-file").innerHTML = "error reading file";
          return false;
      }
    }
    else  return false;

    return true;

}

let activateSensor = function(id){

  if ( document.getElementById('btn-active-sens-'+id).innerHTML == 'Activer'){
    ipc.send('activate-sensor', id);
    console.log("sensor activated");
    document.getElementById('btn-active-sens-'+id).innerHTML = 'Désactiver'
  }
  else {

    ipc.send('disactivate-sensor', id);
    console.log("sensor disactivated");
    document.getElementById('btn-active-sens-'+id).innerHTML = 'Activer'

  }
}


actionneur_checked.addEventListener("input", () => {
  is_actuator = 1;
  document.getElementById('interval').style.display = 'none';

});

not_actionneur_checked.addEventListener("input", () => {
  is_actuator = 0;
  document.getElementById('interval').style.display = 'block';

});

module.exports.removeSensor = removeSensor;
module.exports.activateSensor = activateSensor;
